Description
-----------
This module allows an interface to remove the apachesolr indexed content(s)/
document(s) of selected bundle. Administrator/Developer can delete the
particular entity id(s)

Why This
--------
Developer can use this module as an example to remove the apachesolr indexed
content(s)/document(s).

Installation
------------
1. Copy the entire permissions_export directory the Drupal sites/all/modules or
   sites/all/modules/contrib directory.

2. Login as an administrator. Enable the module in the "Administer" ->
   "Configuration" -> Search and metadata -> Purge Content(s)

3. Access the links to purge the indexed entity content(s)/document(s)
   (admin/config/search/apachesolr/purge-solr-content)

DEPENDENCIES:
-------------
apachesolr

AUTHOR:
-------
Devendra Yadav <dev.firoza@gmail.com>
